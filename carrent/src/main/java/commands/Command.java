package commands;

public enum Command {
    LISTCARS,
    LISTCUSTOMERS,
    FINDBRAND,
    UNKNOWN,
    ADDCAR,
    ADDCUSTOMER,
    DELETE,
    UPDATE,
    EXIT,
    FINDCUSTOMER,


}
