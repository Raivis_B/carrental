package cars;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "car")
public class Cars {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private Integer carId;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "gearbox_type")
    private String gearboxType;

    @Column(name = "car_class")
    private String carClass;

    @Column(name = "mileage")
    private Double mileage;

    @Column(name = "price_per_day")
    private Double pricePerDay;

    @OneToMany(mappedBy = "cars", fetch = FetchType.EAGER)
    private Set<Cars> cars = new HashSet<>();


    public Cars() {

    }

    public Cars(Integer carId, String brand, String model, String gearboxType, String carClass, Double mileage, Double pricePerDay) {
        this.carId = carId;
        this.brand = brand;
        this.model = model;
        this.gearboxType = gearboxType;
        this.carClass = carClass;
        this.mileage = mileage;
        this.pricePerDay = pricePerDay;
    }

    public Set<Cars> getCars() {
        return cars;
    }

    public void setCars(Set<Cars> cars) {
        this.cars = cars;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGearboxType() {
        return gearboxType;
    }

    public void setGearboxType(String gearboxType) {
        this.gearboxType = gearboxType;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "carId=" + carId +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", gearboxType='" + gearboxType + '\'' +
                ", carClass='" + carClass + '\'' +
                ", mileage=" + mileage +
                ", pricePerDay=" + pricePerDay +
                '}';
    }
}
