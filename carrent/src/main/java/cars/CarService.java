package cars;

import java.time.LocalDate;
import java.util.List;

public class CarService {
    private CarRepository carRepository;

    public CarService() {
        this.carRepository = new CarRepository();
    }

    public List<Cars> listCars() {
        //šeit nekādi aprēķini nav, bet tiek atgriezts rezultāts pa taisno no repository
        return carRepository.findAll();
    }

    public List<Cars> findByBrand(String brand) {
        return carRepository.findByBrand(brand);
    }

    public Cars findById(Integer carId) {
        return carRepository.findByID(carId);
    }


    public void saveCar(Cars car) {
        carRepository.save(car);
    }

    public void updateCar(Cars car) {
        carRepository.update(car);
    }

    public void deleteCar(Cars car) {
        carRepository.delete(car);
    }

}
