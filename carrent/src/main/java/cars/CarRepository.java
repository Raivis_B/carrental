package cars;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import session.SessionFactoryHolder;
import org.hibernate.query.Query;


import java.util.List;

public class CarRepository {
    public List<Cars> findAll() {
        Session session = SessionFactoryHolder.openSession();
        List<Cars> cars = session.createQuery("from Cars", Cars.class)
                .getResultList();
        session.close();
        return cars;
    }

    public List<Cars> findByBrand(String brand) {
        Session session = SessionFactoryHolder.openSession();
        Query<Cars> query = session.createQuery("from Cars c WHERE c.brand = :brand", Cars.class);
        query.setParameter("brand", brand);
        List<Cars> cars = query.getResultList();
        session.close();
        return cars;
    }

    public Cars findByID(Integer carId) {
        Session session = SessionFactoryHolder.openSession();
        Cars car = session.find(Cars.class, carId);
        session.close();
        return car;
    }


    public Cars save(Cars car) {
        Session session = SessionFactoryHolder.openSession();
        session.beginTransaction();
        session.save(car);
        session.getTransaction().commit();
        session.close();
        return car;
    }

    public Cars update(Cars car) {
        Session session = SessionFactoryHolder.openSession();
        session.beginTransaction();
        session.update(car);
        session.getTransaction().commit();
        session.close();
        return car;
    }

    public Cars delete(Cars car) {
        Session session = SessionFactoryHolder.openSession();
        session.beginTransaction();
        session.delete(car);
        session.getTransaction().commit();
        session.close();
        return car;
    }


}


