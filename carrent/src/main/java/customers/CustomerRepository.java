package customers;

import cars.Cars;
import org.hibernate.Session;
import org.hibernate.query.Query;
import session.SessionFactoryHolder;

import java.util.List;

public class CustomerRepository {

    public List<Customer> findAll() {
        Session session = SessionFactoryHolder.openSession();
        List<Customer> customers = session.createQuery("from Customer", Customer.class)
                .getResultList();
        session.close();
        return customers;
    }

    public Customer save(Customer customer) {
        Session session = SessionFactoryHolder.openSession();
        session.beginTransaction();
        session.save(customer);
        session.getTransaction().commit();
        session.close();
        return customer;
    }

    public Customer delete(Customer customer) {
        Session session = SessionFactoryHolder.openSession();
        session.beginTransaction();
        session.delete(customer);
        session.getTransaction().commit();
        session.close();
        return customer;
    }

    public Customer findId(Integer customerId) {
        Session session = SessionFactoryHolder.openSession();
        Customer customer = session.find(Customer.class, customerId);

        session.close();
        return customer;
    }


}
