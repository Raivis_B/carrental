package customers;

import cars.CarRepository;
import cars.Cars;

import java.util.List;

public class CustomerService {
    private CustomerRepository customerRepository;

    public CustomerService() {
        this.customerRepository = new CustomerRepository();
    }

    public List<Customer> listCustomers() {
        //šeit nekādi aprēķini nav, bet tiek atgriezts rezultāts pa taisno no repository
        return customerRepository.findAll();
    }

    public void saveCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    public Customer findCustById(Integer customerId) {
        return customerRepository.findId(customerId);
    }

}
