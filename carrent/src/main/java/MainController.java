import cars.CarRepository;
import cars.CarService;
import cars.Cars;
import customers.CustomerService;
import customers.Customer;
import commands.Command;

import java.util.Scanner;

public class MainController {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CarService service = new CarService();
        CustomerService customerService = new CustomerService();

        while (true) {
            System.out.println("Ievadiet komandu:");
            String inputInformation = scanner.nextLine();

            Command command = Command.UNKNOWN;

            for (Command cmd : Command.values()) {
                if (cmd.name().equals(inputInformation.toUpperCase())) {
                    command = cmd;
                }
            }

            switch (command) {
                case EXIT:
                    System.exit(1);
                    break;
                case LISTCARS:
                    service.listCars().forEach(System.out::println);
                    break;
                case LISTCUSTOMERS:
                    customerService.listCustomers().forEach(System.out::println);
                    break;
                case ADDCUSTOMER:
                    Customer customer = new Customer();
                    System.out.println("Your name");
                    customer.setName(scanner.nextLine());
                    System.out.println("Your surname");
                    customer.setSurname(scanner.nextLine());
                    System.out.println("Your credit card limit");
                    customer.setCreditCardLimit(scanner.nextDouble());
                    customerService.saveCustomer(customer);
                    break;
                case ADDCAR:
                    Cars cars = new Cars();
                    System.out.println("Input car brand");
                    cars.setBrand(scanner.nextLine());
                    System.out.println("Input car model");
                    cars.setModel(scanner.nextLine());
                    System.out.println("Input gearbox type");
                    cars.setGearboxType(scanner.nextLine());
                    System.out.println("Input car car class");
                    cars.setCarClass(scanner.nextLine());
                    System.out.println("Input mileage");
                    cars.setMileage(scanner.nextDouble());
                    System.out.println("Input price per day");
                    cars.setPricePerDay(scanner.nextDouble());
                    service.saveCar(cars);
                    break;
                case DELETE:
                    System.out.println("Objects that can be deleted:"
                            + "\n1. Cars;"
                            + "\n2. Customers;"
                            + "\n"
                            + "\nInput the number of object you want to delete");
                    int delfield = scanner.nextInt();
                    switch (delfield) {
                        case 1:
                            System.out.println("Input car brand to find cars you want to delete");
                            String findCarToDelete = scanner.next();
                            service.findByBrand(findCarToDelete).forEach(System.out::println);
                            System.out.println("Input car ID you want to delete");
                            Cars carDelete = service.findById(scanner.nextInt());
                            service.deleteCar(carDelete);
                            System.out.println("Car " + carDelete + " was deleted!");
                            break;
                        case 2:
                            customerService.listCustomers().forEach(System.out::println);
                            System.out.println("Input customer Id to delete");
                            Customer customerDelete = customerService.findCustById(scanner.nextInt());
                            customerService.delete(customerDelete);
                            System.out.println("Custome: " + customerDelete + " was deleted!");
                            break;
                        default:
                            System.out.println("Object doesn't exist!");
                    }
                    break;
                case FINDBRAND:
                    System.out.println("Input car brand to find cars");
                    String findCar = scanner.nextLine();
                    service.findByBrand(findCar).forEach(System.out::println);
                    break;
                case FINDCUSTOMER:
                    System.out.println("Input customer Id to find customer");
                    int findCustomer = scanner.nextInt();
                    System.out.println(customerService.findCustById(findCustomer));

                    break;
                case UPDATE:
                    System.out.println("Input car brand to find cars you want to update");
                    String findCarToUpdate = scanner.nextLine();
                    service.findByBrand(findCarToUpdate).forEach(System.out::println);
                    System.out.println("Input car ID you want to update");
                    Cars car = service.findById(scanner.nextInt());
                    System.out.println("Fields that can be updated:"
                            + "\n1. Mileage;"
                            + "\n2. Price per day;"
                            + "\n"
                            + "\nInput the number of field you want to update");
                    int field = scanner.nextInt();
                    switch (field) {
                        case 1:
                            System.out.println("Input new mileage");
                            car.setMileage(scanner.nextDouble());
                            service.updateCar(car);
                            System.out.println("Car was updated" + car);
                            break;
                        case 2:
                            System.out.println("Input new price per day");
                            car.setPricePerDay(scanner.nextDouble());
                            service.updateCar(car);
                            System.out.println("Car was updated" + car);
                            break;
                        default:
                            System.out.println("Field doesn't exist!");
                    }
                    break;
                default:
                    System.out.println("Invalid command, try another!");
            }
        }
    }

}
