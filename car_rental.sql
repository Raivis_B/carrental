
create database car_rental;
use car_rental;
create table car(car_id int not null auto_increment
				,brand varchar(50)
                ,model varchar(50)
                ,gearbox_type varchar(50)
                ,car_class varchar(50)
                ,mileage double
                ,price_per_day double
                ,primary key car_pk(car_id)
                );
                

                
create table customer(customer_id int not null auto_increment
				,name varchar(50)
                ,surname varchar(50)
                ,credit_card_limit double
                ,primary key cutomer_pk(customer_id)
                );
                
insert into customer(name, surname, credit_card_limit)
		values('Testuser','Testsurname',1550.0);
        

create table reservations(reservation_id int not null auto_increment
				,car_id int
                ,customer_id int
                ,pick_up_date date
                ,return_date date
                ,mileage_on_return double
                ,primary key reservations_pk(reservation_id)
                ,constraint reservations_fk1 foreign key (car_id)  references car(car_id)
                ,constraint reservations_fk2 foreign key (customer_id)  references customer(customer_id)
                );
                

insert into car(brand, model, gearbox_type, car_class, mileage, price_per_day)
		values('Mitsubishi', 'Mirage', 'Automatic','Economy', 35000.5, 42.5)
			;
            
            
insert into car(brand, model, gearbox_type, car_class, mileage, price_per_day)
		values('Nissan', 'Versa', 'Automatic','Compact', 15785.2, 53.2)
			 ,('Hyundai', 'Elantra', 'Automatic','Midsize', 6450.5, 48.0)
			 ,('Volkswagen', 'Jetta', 'Automatic','Compact', 2500.5, 37.25)
			 ,('Ford', 'Eco Sport', 'Automatic','SUV', 8056.8, 65.0)
			 ,('Audi', 'Q3', 'Automatic','SUV', 12345.2, 67.0)
			 ,('BMW', 'X3', 'Automatic','SUV', 150.5, 70.0)
			 ,('Toyota', 'Rav4', 'Automatic','SUV', 9755.3, 65.25)
			 ,('Audi', 'Q5', 'Automatic','Premium SUV', 3564.0, 78.0)
			 ,('BMW', 'X5', 'Automatic','Premium SUV', 1841.2, 84.0)
			 ,('BMW', 'X5', 'Automatic','Premium SUV', 9855.2, 84.0)
			;
            

                
